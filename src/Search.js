import queryString from "query-string"
import React, { useState, useEffect } from 'react';
import './Search.css';
import  axios from 'axios';
import { Link, withRouter  } from 'react-router-dom';
import { connect } from "react-redux"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Addtocartthunk } from "../src/reduxstore/thunks"


function Search(props){
  const [cakes, setCakes] = useState([]);
  let query = queryString.parse(props.location.search)
  // console.log("query is" , props)

  useEffect(()=>{
    let apiurl = process.env.REACT_APP_BASE_API_URL + "/searchcakes?q="+ query.q
    axios.get(`https://apifromashu.herokuapp.com/api/searchcakes?q=${query.q}`)
    .then((response) => {
      if(response.data.data.length == 0){
        toast("No cake found")
      }else{
        setCakes(response.data.data)
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  },[query.q])

  const cart = (res) => {
    if(!localStorage.getItem('token')){
      alert("you need to login frist")
    }else{
      let cakedata = {
        name:res.name,
        cakeid:res.cakeid,
        price:res.price,
        weight:res.weight,
        image:res.image
      }

      props.dispatch(
        Addtocartthunk(cakedata)
      ).then(()=>{
        props.dispatch({
          type:"getcartitems",
        })

        toast('Cake Added Successfully Done')

      })
    }   
  }

  return (
    <div className="container">
      Search for {query.q}
      <div className="row">
        {cakes.length == 0 && <div className="alert alert-warning col-lg-12 col-md-12 col-sm-12 col-xs-12" style={{ margin: '50px auto'}} role="alert">
          Opps no cake found
        </div>}
        {cakes.length >0 && cakes.map((res)=>{
        return <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div className="my-list">
            <img style={{height:'200px',width:'300px'}} src={res.image} alt={res.name} />
            <div className="offer" style={{marginTop: 50+'px'}}><Link to={`/cake/${res._id}`}>{res.name}</Link></div>
            <div className="offer">{res.price} ₹</div>
            <div className="offer">
              <a onClick={() => cart(res)} className="btn btn-info">Add To Cart</a>
            </div>
          </div>
        </div>
        })}
        <ToastContainer />
      </div>
    </div>
    
  )
}


Search = withRouter(Search)
export default connect((state,props)=>{
  return {
    // cakes: state['cakeReducer']['data'],
    cartcount : state['cartReducer']['cartcount'],
    cartdata : state['cartReducer']['Cart_Items']
  }
})(Search);
