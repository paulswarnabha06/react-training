import queryString from "query-string"
import React, { useState, useEffect } from 'react';
import  axios from 'axios';
import { useLocation } from "react-router-dom";
import { Link ,withRouter } from 'react-router-dom';
import { connect } from "react-redux"
import { Addtocartthunk } from "../src/reduxstore/thunks"


function Cart(props){

    // function remove(name) {
    //     props.dispatch({
    //         type:"deleteItemFromCart",
    //         payload:{'cakeid': name.cakeid}
    //     })
    //     if(props.isloading){
    //         props.dispatch({
    //             type:"getcartitems",
    //         })
    //     }
    // }

    function increaseQuantity(res){
        res.quantity = res.quantity + 1
        res.price = res.price * 2

        props.dispatch(
            Addtocartthunk(res)
        )
    }

    function decreaseQuantity(res){
        res.quantity = res.quantity - 1

        props.dispatch({
            type:"removeCart",
            payload:res
        })
        
    }

    
    useEffect(()=>{

        if(!localStorage.getItem('token')){
            props.history.push("/login")
        }else{

            props.dispatch({
                type:"getcartitems",
            })
        }

    },[])

  return (
    <div className="container" style={{maxWidth:"100%",marginTop:"50px",marginLeft:"70px"}}>
        <div className="row">
            <div className="col-sm-12 col-md-10 col-md-offset-1">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th className="text-center">Price</th>
                            <th className="text-center">Total</th>
                            <th className="text-center">Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    {props && props.cartdata.length == 0 &&
                        <div>
                        <p>No Cart Data found</p>
                        </div>
                    }

                    {props && props.cartdata.length > 0 && props.cartdata.map((res,i)=>{
                        
                        return (
                            <tr key={i}>
                                <td className="col-sm-8 col-md-6">
                                <div className="media">
                                    <a className="thumbnail pull-left" href="#"> 
                                    <img className="media-object" src={res.image} style={{width: "72px", height: "72px"}}/> 
                                    </a>
                                    <div className="media-body">
                                        <h4 className="media-heading"><a href="#">{res.name}</a></h4>
                                        <span>Weight: </span><span className="text-success"><strong>{res.weight}</strong></span>
                                    </div>
                                </div></td>
                                <td className="col-sm-1 col-md-1" style={{textAlign: "center"}}>
                                    <input 
                                        type="text" 
                                        className="form-control" 
                                        name="quentity" 
                                        value={res.quantity}
                                    />
                                    
                                
                                </td>
                                <td className="col-sm-1 col-md-1 text-center">
                                    <strong>{res.price}</strong>
                                </td>
                                <td className="col-sm-1 col-md-1 text-center">
                                    <strong>{res.price * res.quantity}</strong>
                                </td>
                                <td className="col-sm-1 col-md-1">
                                    <button 
                                        className="btn btn-info btn-sm"
                                        onClick={() => increaseQuantity(res)}
                                        style={{margin:"10px"}}
                                    >
                                        +
                                    </button>
                                    <button 
                                        className="btn btn-danger btn-sm"
                                        onClick={() => decreaseQuantity(res)}
                                    >
                                        -
                                    </button>
                                    {/* <button 
                                        type="button" 
                                        className="btn btn-danger"
                                        onClick={() => remove(res)}
                                    >
                                        <span className="glyphicon glyphicon-remove"></span> Remove
                                    </button> */}
                                </td>
                            </tr>
                        )
                    })}
                        
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td>   </td>
                            <td><h3>Total</h3></td>
                            <td className="text-right"><h3><strong>{props.carttotal}</strong></h3></td>
                        </tr>
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td>   </td>
                            <td>
                            <button type="button" className="btn btn-default" onClick={() => props.history.push('/')}>
                                <span className="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                            </button></td>
                            <td>
                                <button type="button" className="btn btn-success" onClick={() => props.history.push('/placeorder')}>
                                    Checkout <span className="glyphicon glyphicon-play"></span>
                                </button>
                            </td>
                           
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  )
}

Cart = withRouter (Cart)
export default connect((state,props) => {
console.log("state>>>>>>>>>>",state)
  return{
    cartdata : state['cartReducer']['cartdata'],
    carttotal : state['cartReducer']['carttotal'],
    isloading: state['cartReducer']['isloading']
  }
})(Cart);
