import { Component } from "react";
import  axios from 'axios';
import { Link, withRouter  } from 'react-router-dom';
import { connect } from "react-redux"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Placeorderthunk } from "../src/reduxstore/thunks"

class Placeorder extends Component{

  constructor(props){
    super(props)
    this.state ={
      name:'', 
      address:'', 
      area:'', 
      city:'',
      pincode:'',
      phone:'',
      cakes:'',
      price:0,

      errorname:'', 
      erroraddress:'', 
      errorarea:'', 
      errorcity:'',
      errorpincode:'',
      errorphone:'',
      errorcakes:'',
      errorprice:''
    }
  }

  componentWillMount(){
    let total = 0;
    let cakes = []
    this.props && this.props.cartdata.map((res)=>{
      total = (res.price * res.quantity) + total
      this.setState({price: total});
      cakes.push(res.name)
      this.setState({cakes : cakes.join()})
    })

    if(this.props.cartdata.length == 0 ){
      window.location.href = '/'
    }
  }

  handleChange = (evt) => {

    if(this.state.name == ''){
      this.setState({errorname: "Name is requeird"})
    }else{
      this.setState({errorname: ""})
    }

    if(this.state.address == ''){
      this.setState({erroraddress: "Address is requeird"})
    }else{
      this.setState({erroraddress: ""})
    }

    if(this.state.area == ''){
      this.setState({errorarea: "Area is requeird"})
    } else{
      this.setState({errorarea: ""})
    }

    if(this.state.city == ''){
      this.setState({errorcity: "City is requeird"})
    } else{
      this.setState({errorcity: ""})
    }

    if(this.state.pincode == ''){
      this.setState({errorpincode: "pincode is requeird"})
    }else{
      this.setState({errorpincode: ""})
    }

    if(this.state.phone == ''){
      this.setState({errorphone: "Phone is requeird"})
    }else{
      this.setState({errorphone: ""})
    }
    this.setState({ [evt.target.name]: evt.target.value });
  }

  handleSubmitevents = (e)=> {
    e.preventDefault()

    console.clear();
    console.log("this.state>>>>>>>>>>>>>>>>>>>>>>",this.state)
    if(this.state.name == ''){
      this.setState({errorname: "Name is requeird"})
    }else{
      this.setState({errorname: ""})
    }

    if(this.state.address == ''){
      this.setState({erroraddress: "Address is requeird"})
    }else{
      this.setState({erroraddress: ""})
    }

    if(this.state.area == ''){
      this.setState({errorarea: "Area is requeird"})
    } else{
      this.setState({errorarea: ""})
    }

    if(this.state.city == ''){
      this.setState({errorcity: "City is requeird"})
    } else{
      this.setState({errorcity: ""})
    }

    if(this.state.pincode == ''){
      this.setState({errorpincode: "pincode is requeird"})
    }else{
      this.setState({errorpincode: ""})
    }

    if(this.state.phone == ''){
      this.setState({errorphone: "Phone is requeird"})
    }else{
      this.setState({errorphone: ""})
    }


    if(
      this.state.name !== '' 
      && this.state.address !== '' 
      && this.state.area !== '' 
      && this.state.city !== '' 
      && this.state.pincode !== '' 
      && this.state.phone !== ''
    ){

      this.props.dispatch(
        Placeorderthunk(this.state)
      ).then(()=>{
        toast('Order Placed Successfully Done')
        setTimeout(()=>{
          window.location.href = '/'
        },3000)
      })
    }else{
      toast('You need to fill all required field')
    }
    
  }

  render(){
    return (
      <div>
        <div className="container">
          <div className="row">
          
            <div className="col-6 col-md-6 col-lg-6" style={{marginTop:'100px',marginLeft: '301px'}}>
              <label>Fullname</label>
              <input 
                type="text" 
                name="name" 
                className="form-control" 
                value={this.state.name} 
                onChange={this.handleChange} 
              />
              <div className="text-danger">{this.state.errorname}</div>

              <label>Address</label>
              
              <input 
                type="text" 
                name="address" 
                className="form-control" 
                value={this.state.address} 
                onChange={this.handleChange} 
              />
              <div className="text-danger">{this.state.erroraddress}</div>


              <label>Area</label>
              <input 
                type="text" 
                name="area" 
                className="form-control" 
                value={this.state.area} 
                onChange={this.handleChange} 
              />
              <div className="text-danger">{this.state.errorarea}</div>


              <label>City</label>
              <input 
                type="text" 
                name="city" 
                className="form-control" 
                value={this.state.city} 
                onChange={this.handleChange} 
              />
              <div className="text-danger">{this.state.errorcity}</div>


              <label>Pincode</label>
              <input 
                type="text" 
                name="pincode" 
                className="form-control" 
                value={this.state.pincode} 
                onChange={this.handleChange} 
              />
              <div className="text-danger">{this.state.errorpincode}</div>


              <label>Phone</label>
              <input 
                type="text" 
                name="phone" 
                className="form-control" 
                value={this.state.phone} 
                onChange={this.handleChange} 
              />
              <div className="text-danger">{this.state.errorphone}</div>
                
                <table className="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Product</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
                        <th scope="col">Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                            {this.props && this.props.cartdata.map((res,i)=>{
                                return (
                                    <tr key={i+"row"}>
                                        <th scope="row">{i+1}</th>
                                        <td>{res.name}</td>
                                        <td>{res.quantity}</td>
                                        <td>{res.price}</td>
                                        <td>{res.price * res.quantity}</td>
                                    </tr>
                                )
                            })}
                            <tr>
                              <th></th>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td>Total : {this.state.price}</td>
                            </tr>
                    
                    </tbody>
                </table>
              <br/>

              <button className="btn btn-primary" type="button" style={{margin: '10px'}} onClick={this.handleSubmitevents} >Place Order</button>
              <button className="btn btn-primary" type="button" style={{margin: '10px'}} onClick={() =>{ this.props.history.push('/cart')} } >Back to cart</button>

            </div>
          
            <ToastContainer />
          </div>
        </div>
      </div>
    );
  }
}

Placeorder = withRouter(Placeorder)
export default connect((state,props) => {
  // console.log("state>>>>>>>>>>in login>>>>>>>>>>>>>>>>>",state)
  return{
    isUserloggedIn : state['authReducer']['isUserloggedIn'],
    name : state['authReducer']['user'] && state['authReducer']['user']['name'],
    token : state['authReducer']['user'] && state['authReducer']['user']['token'],
    cartcount : state['cartReducer']['cartcount'],
    cartdata : state['cartReducer']['cartdata']
  }
})(Placeorder);
