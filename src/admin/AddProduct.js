import { Component } from "react";
import  axios from 'axios';
import { Link, withRouter  } from 'react-router-dom';
import { connect } from "react-redux"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Addproductthunk } from "../../src/reduxstore/thunks"


class AddProduct extends Component{

  constructor(props){
    super(props)
    this.state ={
      ingredients:[],
      ingredientsarr:[],
      fields:[],
      file:"https://res.cloudinary.com/lekhisahabdev/image/upload/v1628401950/cvsj9035rc56ygwu9hft.jpg",
      imageurl:localStorage.imageUrl 
    }
    this.handleChange = this.handleChange.bind(this);
    console.log(localStorage.imageUrl )
  }

  addInput = ev => {
    this.setState(prev => ({ ingredients: [...prev.ingredients, 'Hi'] }))
  }

  handleChangeImageUpload = (event) => {
    const file = event.target.files[0]
    console.log('PHOTO:>>>>>>>>>>>>>>>>>>', file);
    const formData = new FormData();
    formData.append("name", file.name);
    formData.append("file", file);

    axios({
      method:"post",
      url:"https://apifromashu.herokuapp.com/api/upload",
      data:formData,
      headers: {authtoken:localStorage.token}
    }).then((response) => {
      if(response.data.imageUrl){
        localStorage.setItem("imageurl",response.data.imageUrl)
        let image = response.data.imageUrl;
        this.setState({"imageurl":image})
        toast("Image Uploaded")
      }
      console.log("response>>>>>>>>>>>>>>>>>>>.",response.data.imageUrl)
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  handleChange = (evt) => {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;
    
    console.log("error>>>>>>>>[]>>>>>>>>>>>>>>",errors)
    this.setState({errors: errors});
    this.setState({ [evt.target.name]: evt.target.value,formIsValid:formIsValid });

  }

  handleChangeIngredients = (evt) => {//ingredient
    // str.includes("world")

    this.setState({ [evt.target.name]: evt.target.value });

    //this.setState(prev => ({ ingredientsarr: [...prev.ingredientsarr, evt.target.value] }))
    console.log(">>ingredientsarr>>>>",this.state)
  }

  handleSubmitevents = (e)=> {
    e.preventDefault()

    let data = {
      image: "https://res.cloudinary.com/lekhisahabdev/image/upload/v1628403533/ha8fj9qqhf9dja6oxyxg.jpg",
      flavour: "Red Velvet",
      price: "695",
      Weight: "1",
      cakename: "Velvet cake",
      ingredients: [
        "Butter",
        " Red valvet"
      ],
      eggless: true
    }

    this.props.dispatch(
      Addproductthunk(data)
    )

    // if(this.state.formIsValid){
    //   var ingredients = this.state.ingredients.split(',');

    //   let data = {
    //     imageurl: this.state.imageurl,
    //     flavour: this.state.flavour,
    //     price: this.state.price,
    //     Weight: this.state.Weight,
    //     name: this.state.cakename,
    //     ingredients: ingredients,
    //     eggless: this.state.eggless == "on" ? true :false
    //   }
    //   console.log("<============================================>",data)
    //   this.props.dispatch(
    //     Addproductthunk(data)
    //   )
    // }else{
    //   console.log("errors>>>>>>>>>>>>>>>>>>>>",this.state.errors)
    //   toast(this.state.errors)
    // }
    
  }

  handleChangeIngrdient(i, event) {
    let values = [...this.state.values];
    values[i] = event.target.value;
    this.setState({ values });
  }

  render(){
    return (
      <div className="container">
        <div className="row">
          <div className="col-6 col-md-6 col-lg-6" style={{marginTop:'100px',marginBottom:'100px',marginLeft: '301px'}}>
                <label for="name" className="col-sm-3 control-label">Cake Image</label>
                <input 
                  type="file" 
                  className="form-control" 
                  name="file" 
                  id="file" 
                  placeholder="Cake Image" 
                  onChange={this.handleChangeImageUpload} 
                />
                { this.state.imageurl && <img src={this.state.imageurl} height="50px" width="50px" />}
                <label for="name" className="col-sm-3 control-label">Cake Name</label>
                <input 
                  type="text" 
                  className="form-control" 
                  name="cakename" 
                  id="cakename" 
                  placeholder="Cake Name" 
                  onChange={this.handleChange} 
                />
                <label for="about" className="col-sm-3 control-label">Description</label>
                <textarea 
                className="form-control" 
                name="description"
                onChange={this.handleChange} 
                ></textarea>

                <label className="control-label small" for="date_start">Weight: </label>
                <input 
                type="text" 
                className="form-control" 
                name="Weight" 
                id="weight" 
                placeholder="weight" 
                onChange={this.handleChange} 
                />

                <label for="price" className="col-sm-3 control-label">Price</label>
                <input 
                type="text" 
                className="form-control" 
                name="price" 
                id="price" 
                placeholder="Price" 
                onChange={this.handleChange} 
                />
            

                <label for="tech" className="col-sm-3 control-label">Type</label>
                <select 
                  className="form-control"
                  name="type"
                  onChange={this.handleChange} 
                >
                    <option value="">Select Cake type</option>
                    <option value="texnolog2">Cake 1</option>
                    <option value="texnolog3">Cake 2</option>
                </select>

                <label for="price" className="col-sm-3 control-label">Eggless</label>
                <input 
                type="checkbox" 
                className="form-control" 
                name="eggless" 
                id="eggless" 
                placeholder="Eggless" 
                onChange={this.handleChange} 
                />
              

                <label className="control-label small" for="date_start">Flavour: </label>
                <input 
                type="text" 
                className="form-control" 
                name="flavour" 
                id="flavour" 
                required
                placeholder="flavour" 
                onChange={this.handleChange} 
                />

              <label for="price" className="col-sm-3 control-label">ingredients</label>
                <input 
                  type="text" 
                  className="form-control" 
                  name="ingredients" 
                  id="ingredients" 
                  required
                  placeholder="Enter ingredients using comma separetor" 
                  onChange={this.handleChange} 
                />
              {/* <div className="abc">
                <a className="btn btn-primary" style={{margin: '10px'}}  onClick={this.addInput}>Add ingredients</a>
                  {this.state.ingredients.map((node,i) => 
                    <input 
                      key={'paul'+i} 
                      style={{margin: '10px'}} 
                      type="text" className="form-control" 
                      // name={'ingredient-'+i} 
                      placeholder="ingredients" 
                      onChange={this.handleChangeIngrdient.bind(this, i)} 
                      // onChange={this.handleChange} //handleChangeIngredients
                    />
                )}
              </div> */}
              <button type="button" style={{margin: '10px'}} onClick={this.handleSubmitevents} className="btn btn-primary">Add Product</button>

              <ToastContainer />
          </div>
        </div>
      </div>
    );
  }
}


AddProduct = withRouter(AddProduct)
export default connect((state,props) => {
  // console.log("state>>>>>>>>>>in login>>>>>>>>>>>>>>>>>",state)
  return{
    productdata : state['productReducer']['productdata'],
    isloading : state['productReducer']['isloading']
  }
})(AddProduct);