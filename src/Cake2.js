import React, { useState, useEffect } from 'react';
//import cakesdata from './cakesdata';
import  axios from 'axios';
import { Link, withRouter  } from 'react-router-dom';
import { connect } from "react-redux"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Cakesthunk,Addtocartthunk } from "../src/reduxstore/thunks"

let cakesdata = [];


export function Cake2(props) {
  const [textInput, setTextInput] = useState(0);

  useEffect(() => {
    if( props && props.data == undefined ){
      props.dispatch(Cakesthunk())
    }
  }, []);

  const handleChange = (event) => {
    setTextInput(event.target.value);
    // if(event.target.value.toString() !== "") setCakes(cakesdata)
  }

  let cakeArray = cakesdata;
  useEffect(() => {  
    setTextInput(props.res)
  });



  const cart = (res) => {
    if(!localStorage.getItem('token')){
      alert("you need to login frist")
    }else{
      // console.log("res>>>>>>>>>>>>>>>>>>",res)
      let cakedata = {
        name:res.name,
        cakeid:res.cakeid,
        price:res.price,
        weight:"1",
        image:res.image
      }

      props.dispatch(
        Addtocartthunk(cakedata)
      ).then(()=>{
        props.dispatch({
          type:"getcartitems",
        })

        toast('Cake Added Successfully Done')

      })
    }   
  }

  return(
    <div className="container">
      <div className="row">
        {props.cakes.length == 0 &&
            <div>
              <p>No Cake found</p>
            </div>
          }
        {props.cakes.length >0 && props.cakes.map((res,i)=>{
          return <div key={i+'item'} style={{ marginTop: 20+'px', marginBottom: 20+'px'}} className="col-12 col-md-4 col-lg-4">
            <div className="card">
                <img className="card-img-top" style={{width: 350+ 'px' , height:  350+ 'px'}} src={res.image} alt="Card image cap"/>
                <div className="card-body">
                    <h4 className="card-title">
                      <Link to={`/cake/${res.cakeid}`}>{res.name}</Link>
                    </h4>
                    
                    <p className="card-text">{res.description}</p>
                    <div className="row">
                        <div className="col">
                            <p className="btn btn-danger btn-block">{res.price} ₹</p>
                        </div>
                        <div className="col">
                            <a 
                              className="btn btn-success btn-block" 
                              onClick={() => cart(res)}
                            >Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        })}
            <ToastContainer />

      </div> 
    </div>
  )
}


Cake2 = withRouter(Cake2)
export default connect((state,props)=>{
  return {
    cakes: state['cakeReducer']['data'],
    cartcount : state['cartReducer']['cartcount'],
    cartdata : state['cartReducer']['Cart_Items']
  }
})(Cake2);