import axios from "axios"

export  function Loginthunk(data){
    return async (dispatch)=>{
        dispatch({
            type:"LOGIN_FETCH"
        })
        var result  = await  axios({
            method:"post",
            url:"https://apifromashu.herokuapp.com/api/login",
            data:data
        })
        if(result.data.token){
            dispatch({
                type:"LOGIN",
                payload:result.data
            })
        }
        else{
            dispatch({
                type:"LOGIN_FAILURE"
            })
        }
       
    }
}

export  function Placeorderthunk(data){
    return async (dispatch)=>{
        dispatch({
            type:"CART_FETCHING"
        })
        var result  = await axios({
            method:"post",
            url:"https://apifromashu.herokuapp.com/api/addcakeorder",
            data:data,
            headers: {authtoken:localStorage.token}
        })
        if(result.data.token){
            dispatch({
                type:"PLACEORDER",
                payload:result.data
            })
        }
        else{
            dispatch({
                type:"CART_FAILURE"
            })
        }
       
    }
}

export  function Cakesthunk(){
    return async (dispatch)=>{
        dispatch({
            type:"CAKE_FETCHING"
        })
        var result  = await axios({
            method:"get",
            url:"https://apifromashu.herokuapp.com/api/allcakes",
            headers: {authtoken:localStorage.token}
        })
        if(result.data){
            console.log("result.data.data>>>>>>>>>>>>>>>>>>>>>>",result.data.data)
            dispatch({
                type:"GETCAKES",
                payload:result.data.data
            })
        }
        else{
            dispatch({
                type:"CAKE_FAILURE"
            })
        }
       
    }
}

export  function Addtocartthunk(data){
    return async (dispatch)=>{
        dispatch({
            type:"CART_FETCHING"
        })
        var result  = await axios({
            method:"post",
            url:"https://apifromashu.herokuapp.com/api/addcaketocart",
            data:data,
            headers: {authtoken:localStorage.token}
        })
        if(result.data){
            dispatch({
                type:"ADDTOCART",
                payload:result.data.data
            })
            dispatch({
                type:"getcartitems",
            })
        }
        else{
            dispatch({
                type:"CART_FAILURE"
            })
        }
       
    }
}

export  function Myorderthunk(){
    return async (dispatch)=>{
        dispatch({
            type:"ORDER_FETCHING"
        })
        var result  = await axios({
            method:"post",
            url:"https://apifromashu.herokuapp.com/api/cakeorders",
            data:{},
            headers: {authtoken:localStorage.token}
        })
        // console.log("result.data.data>>>>>>>myorder thunk>>>>>>>>>>>>>>>",result.data.cakeorders)
        if(result.data.cakeorders){
            // console.log("result.data.data>>>>>>in if condition>>>>>>>>>>>>>>>>",result.data.cakeorders)
            dispatch({
                type:"MY_ORDER",
                payload:result.data.cakeorders
            })
        }
        else{
            dispatch({
                type:"ORDER_FAILURE"
            })
        }
       
    }
}


export  function Imageuploadthunk(data){
    return async (dispatch)=>{
        dispatch({
            type:"PRODUCT_FETCHING"
        })
        var result  = await axios({
            method:"post",
            url:"https://apifromashu.herokuapp.com/api/cakeorders",
            data:{data},
            headers: {authtoken:localStorage.token}
        })
        if(result.data.imageUrl){
            localStorage.setItem("imageurl",result.data.imageUrl)
            dispatch({
                type:"IMAGE_UPLOAD",
                payload:result.data.imageUrl
            })
        }
        else{
            dispatch({
                type:"PRODUCT_FAILURE"
            })
        }
       
    }
}


export  function Addproductthunk(data){

    console.log("Addproductthunk>>>>>>>>>>>>>>>>>>>>>>>>>>>",Addproductthunk)

    return async (dispatch)=>{
        dispatch({
            type:"PRODUCT_FETCHING"
        })
        var result  = await axios({
            method:"post",
            url:"https://apifromashu.herokuapp.com/api/addcake",
            data:{data},
            headers: {authtoken:localStorage.token}
        })
        console.log(result,">>>>>>>>>>>result>>>>>>>>>>>>>>>")
        if(result.data){
            dispatch({
                type:"ADD_PRODUCT",
                payload:result.data
            })
        }
        else{
            dispatch({
                type:"PRODUCT_FAILURE"
            })
        }
       
    }
}

// export  function Cartbyidthunk(data){
//     return async (dispatch)=>{
//         dispatch({
//             type:"CART_FETCHING"
//         })
//         var result  = await axios({
//             method:"post",
//             url:"https://apifromashu.herokuapp.com/api/addcaketocart",
//             data:data,
//             headers: {authtoken:localStorage.token}
//         })
//         if(result.data){
//             dispatch({
//                 type:"ADDTOCART",
//                 payload:result.data.data
//             })
//         }
//         else{
//             dispatch({
//                 type:"CART_FAILURE"
//             })
//         }
       
//     }
// }

// export  function Myorderthunk(){
//     return async (dispatch)=>{
//         dispatch({
//             type:"ORDER_FETCHING"
//         })
//         var result  = await axios({
//             method:"post",
//             url:"https://apifromashu.herokuapp.com/api/cakeorders",
//             data:{},
//             headers: {authtoken:localStorage.token}
//         })
//         if(result.data.cakeorders){
//             console.log("orderthunk>>>>>>>>>>",result.data.cakeorders)
//             dispatch({
//                 type:"MYORDER",
//                 payload:result.data.cakeorders
//             })
//         }
//         else{
//             console.log("else orderthunk>>>>>>>>>>")

//             dispatch({
//                 type:"ORDER_FAILURE"
//             })
//         }
       
//     }
// }