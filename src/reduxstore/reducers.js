export var productReducer =function(
    state={
        productdata:[],
    },
    action
    ){
    console.log("action og order>>>>>>>>>>>>",action)
    switch(action.type){
        
        case "PRODUCT_FETCHING" :{
            state = {...state}
            state["isloading"] = true
            return state
        }
        case "ADD_PRODUCT":{
            state = {...state}
            state["isloading"] = false
            state['productdata'] = action.payload
            // console.log(action.payload,">>>>>>>>>>MYORDER>>>>>>>>>>",state)
            return state;
        } 
        case "IMAGE_UPLOAD":{
            state = {...state}
            state["isloading"] = false
            state['imageurl'] = action.payload
            // console.log(action.payload,">>>>>>>>>>MYORDER>>>>>>>>>>",state)
            return state;
        } 
        case "PRODUCT_FAILURE" :{
            state = {...state}
            state["isloading"] = false
            state["ordererror"] = "Some Error Occurred"
            return state
        }
        default : return state
    }
}


export var orderReducer =function(
    state={
        orderdata:[],
    },
    action
    ){
    console.log("action og order>>>>>>>>>>>>",action)
    switch(action.type){
        
        case "ORDER_FETCHING" :{
            state = {...state}
            state["isloading"] = true
            return state
        }
        case "MY_ORDER":{
            state = {...state}
            state["isloading"] = false
            state['orderdata'] = action.payload
            // console.log(action.payload,">>>>>>>>>>MYORDER>>>>>>>>>>",state)
            return state;
        } 
        case "ORDER_FAILURE" :{
            state = {...state}
            state["isloading"] = false
            state["ordererror"] = "Some Error Occurred"
            return state
        }
        default : return state
    }
}

export var cartReducer =function(state={
    cartcount:localStorage.cartcount ? localStorage.cartcount : 0,
    cartdata:localStorage.cartdata ? JSON.parse(localStorage.cartdata) : [],
},action){
switch(action.type){
    
    case "CART_FETCHING" :{
        state = {...state}
        state["isloading"] = true
        return state
    }
    case "ADDTOCART":{
        state = {...state}
        state["isloading"] = false
        state['addtocart'] = action.payload
        console.log("ADDTOCART>>>>>>>>>>>>>>>",state)
        return state;
    } 
    case "DELETECAKEFORMCART":{
        state = {...state}
        state["isloading"] = false
        state['removedata'] = action.payload
        console.log("DELETECAKEFORMCART>>>>>>>>>>>>>>>",state)
        return state;
    } 
    case "REMOVECAKEFROMCART":{
        state = {...state}
        state["isloading"] = false
        state['removedata'] = action.payload
        console.log("REMOVECAKEFROMCART>>>>>>>>>>>>>>>",state)
        return state;
    }
    case "CARTDETAILS":{
        state = {...state}
        let total = 0;
        action.payload.map((res)=>{
            total = (res.price * res.quantity) + total
        })
        state["isloading"] = false
        state['cartcount'] = action.payload && action.payload.length
        state['cartdata'] = action.payload
        state['carttotal'] = total
        return state;
    } 
    case "PLACEORDER":{
        state = {...state}
        state["isloading"] = false
        state['placeorder'] = action.payload
        return state;
    } 
    case "GETCARTBYID":{
        state = {...state}
        state["isloading"] = false
        state['cartdetails'] = action.payload
        return state;
    } 
    case "CART_FAILURE" :{
        state = {...state}
        state["isloading"] = false
        state["carterror"] = "Some Error Occurred"
        return state
    }
    default : return state
}
}


export var cakeReducer =function(state={
        data:localStorage.data ? JSON.parse(localStorage.data) : []
    },action){
    // console.log("action>>>>>>>>>>>>>>>>>",action)
    switch(action.type){
        case "CAKE_FETCHING" :{
            state = {...state}
            state["isloading"] = true
            return state
        }
        case "GETCAKES":{
            state = {...state}
            state['data'] = action.payload
            return state;
        } 
        case "CAKE_FAILURE" :{
            state = {...state}
            state["isloading"] = false
            state["cakeerror"] = "Some Error Occurred"
            return state
        }
        default : return state
    }
}

export var authReducer =function(state={
    isUserloggedIn:localStorage.token ? true : false,
    isloading:false
},action){
    switch(action.type){
        case "LOGIN_FETCH" :{
            state = {...state}
            state["isloading"] = true
            return state
        }
        case "LOGIN":{
            state = {...state}
            state['isUserloggedIn'] = true
            state['user'] = action.payload
            console.log("LOGIN>>>>>>>>>>>>>>>>>",state)

            return state;
        } 
        case "LOGIN_FAILURE" :{
            state = {...state}
            state["isloading"] = false
            state["loginerror"] = "Some Error Occurred"
            console.log("LOGIN_FETCHING>>>>>>>>>>>>>>>>>",state)

            return state
        }
        default : return state
    }
}

// export var attendTraning =function(state={employee:10},action){

//     switch(action.type){
//         case "SOMEACTION":{
//             state = {...state}
//             return state;
//         }
//         case "TOTAL_PARTICIPANTS":{
//             state = {...state}
//             state['employee']+=1
//             return state;
//         }    
//         default : return state
//     }
// }

// export var leftTraning =function(state={leftemployee:20},action){

//     switch(action.type){
//         case "SOMEACTION":{
//             state = {...state}
//             return state;
//         }
//         case "LEFT_PARTICIPANTS":{
//             state = {...state}
//             state['leftemployee']-=1
//             return state;
//         }    
//         default : return state
//     }
// }