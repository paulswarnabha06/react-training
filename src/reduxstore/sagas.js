
import {all , takeEvery , put} from "redux-saga/effects"
import axios from "axios"
function *Swarnabha({ payload }){
  // first make and async task  and based on result dispatch necessary action
  var success = true
  yield put({
      type:"CART_FETCHING"
  })

  var response  = yield axios({method:"post",
   url:"https://apifromashu.herokuapp.com/api/cakecart",
   data:{ payload }
  })
  console.log("data  from cart items " , response)
  if(response.data.data){
      yield put({
        type:"CARTDETAILS",
        payload:response.data.data
    })
  }else{
    yield put({
        type:"CART_FAILURE"
    })
  }
}


function *CartSaga(){
  yield takeEvery( 'getcartitems' , Swarnabha)//CartGenerator
}

/////////////////////////////////////////////////////////////////

function *DeleteCart({ payload }){
  // first make and async task  and based on result dispatch necessary action
  var success = true
  yield put({
      type:"CART_FETCHING",payload
  })
  console.log("=============DeleteCart=payload=========== " , payload)

  var response  = yield axios({method:"post",
   url:"https://apifromashu.herokuapp.com/api/removecakefromcart",
   data:payload
  })
  console.log("data  from cart items " , response)
  if(response.data.data){
      yield put({
        type:"DELETECAKEFORMCART",
        payload:response.data.data
    })
  }else{
    yield put({
        type:"CART_FAILURE"
    })
  }
}


function *DeleteCartSaga(){
  yield takeEvery( 'deleteItemFromCart' , DeleteCart)//CartGenerator
}

///////////////////////////////////////////////////////////////

/////////////////////////////Start Remove cart////////////////////////////////////

function *RemoveCart({ payload }){
  var success = true
  yield put({
      type:"CART_FETCHING",
      payload
  })
  console.log("==============RemoveCart=========== " , payload)

  var response  = yield axios({method:"post",
   url:"https://apifromashu.herokuapp.com/api/removeonecakefromcart",
   data:payload
  })
  console.log("data  from cart items " , response)
  if(response.data.message){

      yield put({
        type:"getcartitems",
      })

      yield put({
        type:"REMOVECAKEFROMCART",
        payload:response.data.data
      })

  }else{
    yield put({
        type:"CART_FAILURE"
    })
  }
}

function *RemoveCartSaga(){
  yield takeEvery( 'removeCart' , RemoveCart)//CartGenerator
}

//////////////////////////////End Remove cart/////////////////////////////////

/////////////////////////////Start My order////////////////////////////////////

// function *MyOrder({ payload }){
//   console.log("<><><><><><><><><>call my order<><><><><><><")
//   var success = true
//   yield put({
//       type:"ORDER_FETCHING",
//       payload
//   })
//   console.log("==============myorder=========== " , payload)

//   var response  = yield axios({method:"post",
//    url:"https://apifromashu.herokuapp.com/api/cakeorders",
//    data:{}
//   })
//   // console.log("data  from my order >>>>>>>>>>>>>>>>>>>>" , response)
//   console.log("????????????????response.data.cakeorders>>>>>>>>",response)

//   if(response.data){
//       yield put({
//         type:"MY_ORDER",
//         payload:response.data.cakeorders
//     })
//   }else{
//     yield put({
//         type:"ORDER_FAILURE"
//     })
//   }
// }

// function *MyOrderSaga(){
//   yield takeEvery( 'myorder' , MyOrder)//CartGenerator
// }

//////////////////////////////End My order/////////////////////////////////


export default function *RootSaga(){
    console.log("root sga ")
 yield all([CartSaga(),DeleteCartSaga(),RemoveCartSaga()])//MyOrderSaga()
}