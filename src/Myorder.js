import { Component } from "react";
import  axios from 'axios';
import {  withRouter  } from 'react-router-dom';
import { connect } from "react-redux"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Myorderthunk } from "../src/reduxstore/thunks"

class Myorder extends Component{

  constructor(props){
    super(props)
    this.state ={
    }
  }

  componentWillMount(){

    // this.props.dispatch(
    //     Myorderthunk()
    // ).then(()=>{
    //     console.log("yhis.props.myorder>>>>>>>>>>>>>>>>>",this.props.myorder)
    // })

    this.props.dispatch(Myorderthunk())
    console.clear()
    // console.log("yhis.props.orderdata>>>>>>>>>>>>>>>>>",this.props.orderdata)
    // console.log("yhis.props.myorder>>>>>>>>>>>>>>>>>",this.props.isloading)

  }


  render(){
    return (
      <div>
        <div className="container">
          <div className="row">
          <div className="text-center" style={{marginTop:'100px',marginLeft:"500px",marginButton:"50px"}}>
            <h1>My Order</h1>
          </div>
          <table className="table table-bordered" >
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Coustomer Name</th>
                <th scope="col">Address</th>
                <th scope="col">Pincode</th>
                <th scope="col">Cakes</th>
                <th scope="col">Price</th>
                <th scope="col">Mode</th>
                <th scope="col">Status</th>
                <th scope="col">Order Date</th>

                </tr>
            </thead>
            <tbody>

              {this.props && this.props.myorder.map((res,i)=>{
                  return (
                      <tr key={i+"row"}>
                          <th scope="row">{i+1}</th>
                          <td>{res.name}</td>
                          <td>{res.address}</td>
                          <td>{res.pincode}</td>
                          <td>{res.cakes.toString()}</td>
                          <td>{res.price}</td>
                          <td>{res.mode}</td>
                          <td>{res.compeleted == false ? "Pending" : "Completed"}</td>
                          <td>{res.orderdate}</td>
                      </tr>
                  )
              })}
                
            </tbody>
            </table>
            <div className="text-center" style={{marginTop:'20px',marginLeft:"500px"}}>
              <button className="btn btn-primary" type="button" onClick={()=>{ window.location.href ="/"}} >Back To Home</button>
            </div>
            <ToastContainer />
          </div>
        </div>
      </div>
    );
  }
}

Myorder = withRouter(Myorder)
export default connect((state,props) => {
    // console.clear()
  console.log("state>>>>>>>>>>myorder>>>>>>>>>>>>>>>>>",state)
  return{
    myorder : state['orderReducer']['orderdata'],
    isloading : state['orderReducer']['isloading'] 
  }
})(Myorder);
